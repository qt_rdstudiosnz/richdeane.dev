import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { SiteProvider } from './context/SiteContext';

ReactDOM.render(
  <React.StrictMode>
    <SiteProvider>
      <App />
    </SiteProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

